package garden.bots

import arrow.core.*
import garden.bots.core.Kompilo
import garden.bots.security.FunkySecurityManager
import garden.bots.tools.*
import io.vertx.core.AbstractVerticle
import io.vertx.ext.healthchecks.HealthCheckHandler
import io.vertx.ext.healthchecks.Status
import io.vertx.ext.web.Router
import io.vertx.ext.web.handler.BodyHandler
import io.vertx.ext.web.handler.StaticHandler
import io.vertx.ext.web.handler.TimeoutHandler
import io.vertx.kotlin.core.http.HttpServerOptions
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj

class Main : AbstractVerticle() {

  override fun start() {

    println("🌼 Booting FunKy-VM...")
    val environment = EnvironmentVariables()

    val router = Router.router(vertx)
    router.route().handler(BodyHandler.create())
    router.route("/*").handler(StaticHandler.create())

    val kompilo = Kompilo()

    when(environment.funktionStorageMode) {
      "file-system" -> {
        getFunktionInformation(
          funktionsPath= environment.funktionsPath,
          funktionToLoad= environment.funktionDirectory
        ).let {
          when(it) {
            is Failure -> println("😡 Houston? ${it.exception.message}")
            is Success -> {
              println("=============================================================================")
              println(" > DataFunktion ${it.value.name}/${it.value.version}")
              println("=============================================================================")
            }
          }
        }
      }

      "object-storage" -> {
        println("=============================================================================")
        println(" > DataFunktion storage id ${environment.funktionStorageId}")
        println("=============================================================================")
      }

    } // --- end of when

    println("=============================================================================")
    println(" > loading common plugins and modules")
    println("=============================================================================")

    // --- Load common plugins (jar files) ---
    loadPluginsFromDisk(
      pluginsPath= environment.pluginsPath,
      kompilo= kompilo
    )

    // --- Load and compile of commons modules
    loadModulesFromDisk(
      modulesPath= environment.modulesPath,
      kompilo= kompilo
    )


    val defineRoutesAndStartServer =  { funktion: DataFunktion, router: Router, kompilo: Kompilo ->

      /* 🤖 === health check === */
      val healthCheck = HealthCheckHandler.create(vertx)

      healthCheck?.register("ping"){ future ->
        //TODO: add some other stuff to be checked
        future.complete(Status.OK())
      }
      // link/bind healthCheck to a route
      router.get("/health/${funktion.name}/${funktion.version}").handler(healthCheck)


      router.get("/funktion").handler {context ->
        context.json(json { obj("function" to "${funktion.name}/${funktion.version}") })
      }

      router.get("/funktion/${funktion.name}/${funktion.version}/:params").handler(TimeoutHandler.create(environment.timeout))
      router.get("/funktion/${funktion.name}/${funktion.version}/:params").blockingHandler { context ->
        kompilo.executeGETFunktion(funktion.name, funktion.version, funktion.type, context)
      }

      router.post("/funktion/${funktion.name}/${funktion.version}").handler(TimeoutHandler.create(environment.timeout))
      router.post("/funktion/${funktion.name}/${funktion.version}").blockingHandler { context ->
        kompilo.executePOSTFunktion(funktion.name, funktion.version, funktion.type, context)
      }

      // when all is loaded
      // TODO: do not write on the disk
      System.setSecurityManager(FunkySecurityManager())

      vertx.createHttpServer(HttpServerOptions(port = environment.httpPort))
        .requestHandler(router)
        .listen { ar -> when {
          ar.failed() -> println("😡 Houston?")
          ar.succeeded() -> println("😃 🌍 ${funktion.name}/${funktion.version} started on ${environment.httpPort}")
        }}
    } // end of defining routes

    when(environment.funktionStorageMode) {
      "file-system" -> {
        // --- Load and compile the function

        /* TODO: add logs of the VM */
        loadFunktionFromDisk(
          funktionsPath= environment.funktionsPath,
          funktionToLoad= environment.funktionDirectory,
          kompilo= kompilo
        ).let {
          when(it) {
            is Failure -> {
              it.exception.printStackTrace()
              println("😡 Houston? ${it.exception.message}")
            }
            is Success -> {
              defineRoutesAndStartServer(it.value, router, kompilo)
            }
          }
        }
      } // --- end of file-system

      "object-storage" -> {

        loadFunktionFromObjectStorage(
          funktionsBucket= environment.funktionsBucket,
          funktionStorageId= environment.funktionStorageId,
          objectStorage= environment.objectStorage,
          accessKey= environment.accessKey,
          secretKey= environment.secretKey,
          kompilo= kompilo
        ). let {
          when(it) {
            is Failure -> {
              it.exception.printStackTrace()
              println("😡 Houston? ${it.exception.message}")
            }
            is Success -> {
              defineRoutesAndStartServer(it.value, router, kompilo)
            }
          }
        }
      } // --- end of object-storage
    } // --- end of when
  } // --- end of start

}
