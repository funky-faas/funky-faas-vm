package garden.bots.allowed

import garden.bots.core.Kompilo
import io.vertx.core.json.JsonObject


// Funktion("hello").version("0.0.2").call(JsonObject().put("name","John Doe"))
@Deprecated("to be defined")
class Funktion(var name: String) {
  private var version = "0.0.0"
  fun version(value: String): Funktion {
    version = value
    return this
  }
  fun call(params: JsonObject): Any {
    return Kompilo().executeFunktion(this.name,this.version,params)
  }
}
