package garden.bots.allowed

import garden.bots.core.Kompilo

/*
### Data class Plugin

`Plugin` is a helper to load class of a jar plugin:

Usage:

```kotlin
Plugin("garden.bots.DarthVader").call("getQuote") as String
```

*/
class Plugin(var name: String? = null) {
  private val klass = Kompilo().loadClass(this.name!!)
  private val instance = klass.getConstructor().newInstance()

  fun call(method: String): Any {
    return klass.getMethod(method).invoke(instance)
  }
  fun <T> call(method: String, vararg args: T ): Any {
    return klass.getMethod(method).invoke(instance)
  }
}