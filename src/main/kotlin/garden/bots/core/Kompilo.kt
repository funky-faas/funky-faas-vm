package garden.bots.core

import arrow.core.*
import garden.bots.html
import garden.bots.json
import garden.bots.param
import garden.bots.text
import garden.bots.tools.JarFileLoader
import io.vertx.core.json.JsonObject
import io.vertx.ext.web.RoutingContext
import io.vertx.kotlin.core.json.json
import io.vertx.kotlin.core.json.obj
import java.net.URL
import javax.script.ScriptEngineManager

/*
https://docs.oracle.com/javase/8/docs/api/javax/script/ScriptEngineManager.html

  👋 you must create a `javax.script.ScriptEngineFactory`file
  in `resources/META-INF.services`
  with this content:

  ```
  org.jetbrains.kotlin.script.jsr223.KotlinJsr223JvmLocalScriptEngineFactory
  ```
*/

@Suppress("UNCHECKED_CAST")
class Kompilo {
  companion object {

    private val classLoader = JarFileLoader(arrayOf<URL>())

    //private val funktions = HashMap<String,(JsonObject)-> JsonObject>()
    private val funktions = HashMap<String,(JsonObject)-> Any>()
    private val funktionsInformation = HashMap<String, JsonObject>()
    private val engine = ScriptEngineManager().getEngineByExtension("kts")

  }

  fun addJar(jarPathFile: String) {
    val res = classLoader.addFile(jarPathFile)
  }

  fun loadClass(className: String): Class<*> {
    return classLoader.loadClass(className)
  }

  fun compileModule(sourceCode: String): Any {
    return Try { engine.eval(sourceCode) }
  }

  fun compileFunktion(funktionInformation: JsonObject, sourceCode: String): Try<(JsonObject)-> Any> {

    return Try { engine.eval(sourceCode) as (JsonObject) -> JsonObject}.let {
      when (it) {
        is Failure -> it
        is Success -> {
          funktionsInformation["${funktionInformation.getString("name")}:${funktionInformation.getString("version")}"] = funktionInformation
          funktions["${funktionInformation.getString("name")}:${funktionInformation.getString("version")}"] = it.value
          it
        }
      }
    }
  }

  fun executeFunktion(funktionName: String, version: String, parameters: JsonObject): Try<Any?> {
    return Try {
      funktions["$funktionName:$version"]?.invoke(parameters)
    }
  }

  fun executeGETFunktion(funktionName: String, version: String, type: String, context: RoutingContext) {
    Try {

      val parameters = JsonObject(context.param("params") as String)

      funktions["$funktionName:$version"]?.invoke(parameters)

    }.let {
      when(it) {
        is Failure -> {
          it.exception.printStackTrace()
          println("🥵 ${it.exception.message}")
          context.json(json { obj("error" to it.exception.message) })
        }
        is Success -> {
          when(type) {
            "json" -> context.json(json { obj("result" to it.value) })
            "text" -> context.text(it.value.toString())
            "html" -> context.html(it.value.toString())
            else -> {
              context.text(it.value.toString())
            }
          }
        }
      }
    }
  }

  fun executePOSTFunktion(funktionName: String, version: String, type: String, context: RoutingContext) {
    Try {

      val parameters = context.bodyAsJson

      funktions["$funktionName:$version"]?.invoke(parameters)

    }.let {
      when(it) {
        is Failure -> {
          it.exception.printStackTrace()
          println("🥵 ${it.exception.message}")
          context.json(json { obj("error" to it.exception.message) })
        }
        is Success -> {
          when(type) {
            "json" -> context.json(json { obj("result" to it.value) })
            "text" -> context.text(it.value.toString())
            "html" -> context.html(it.value.toString())
            else -> {
              context.text(it.value.toString())
            }
          }
        }
      }
    }
  }

}