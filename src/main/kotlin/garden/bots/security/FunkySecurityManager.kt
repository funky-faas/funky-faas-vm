package garden.bots.security

import java.lang.SecurityManager
import java.security.Permission

//open class SecurityManager
//http://www.java2s.com/Tutorial/Java/0490__Security/Defineyourownsecuritymanager.htm

class FunkySecurityManager : SecurityManager {
  constructor(): super() {}

  override fun checkExit(status: Int) {
    super.checkExit(status)
    throw Exception("😡 exit is ⛔️ [${this.securityContext.toString()}]")
    //throw java.lang.SecurityException("😡 exit is ⛔️ [${this.securityContext.toString()}]")
  }

  /* TODO: read, write */

  override fun checkPermission(perm: Permission?, context: Any?) {
    //super.checkPermission(perm, context)
    //println("permission + cxt: $perm.toString()")
  }

  override fun checkPermission(perm: Permission?) {
    //super.checkPermission(perm)
    //println("permission: $perm.toString()")
  }

  override fun checkPackageAccess(pkg: String?) {

    super.checkPackageAccess(pkg)
    if (pkg != null) {


      if(pkg.startsWith("garden.bots.core")) {
        /*
        println("package access: $pkg")
        println(this.securityContext.javaClass.canonicalName)
        this.classContext.forEach { it ->
          println("- ${it.canonicalName}")
        }
        */
        throw Exception("😡 a funktion is using $pkg")
      }

      if(pkg.startsWith("garden.bots.security")) {
        throw Exception("😡 a funktion is using $pkg")
      }
      if(pkg.startsWith("garden.bots.tools")) {
        throw Exception("😡 a funktion is using $pkg")
      }

    }
  }

  override fun checkPackageDefinition(pkg: String?) {
    //super.checkPackageDefinition(pkg)
    //println("package definition: $pkg")
  }

  override fun checkAccess(t: Thread?) {
    //super.checkAccess(t)
    //println("> checkAccess t: ${t.toString()}")
  }


}