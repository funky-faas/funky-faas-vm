package garden.bots.tools

import arrow.core.None
import arrow.core.Option
import arrow.core.Some

class EnvironmentVariables {

  val httpPort: Int = Option.fromNullable(System.getenv("VM_HTTP_PORT")).let { when(it) {
    is None -> 9090
    is Some -> Integer.parseInt(it.t)
  }}

  val timeout: Long = Option.fromNullable(System.getenv("VM_TIMEOUT")).let { when(it) {
    is None -> 1000
    is Some -> Integer.parseInt(it.t).toLong()
  }}

  val funktionStorageMode: String = Option.fromNullable(System.getenv("VM_STORAGE_MODE")).let { when(it) {
    is None -> "file-system"
    is Some -> it.t
  }}

  // --- object storage

  val funktionsBucket: String = Option.fromNullable(System.getenv("VM_FUNKTIONS_BUCKET")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}
  val accessKey: String = Option.fromNullable(System.getenv("VM_ACCESS_KEY")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}
  val secretKey: String = Option.fromNullable(System.getenv("VM_SECRET_KEY")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}
  val objectStorage: String = Option.fromNullable(System.getenv("VM_OBJECT_STORAGE")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}
  val funktionStorageId: String = Option.fromNullable(System.getenv("VM_FUNKTION_STORAGE_ID")).let { when(it) {
    is None -> ""
    is Some -> it.t
  }}

  // --- file system ---
  val funktionsPath: String = Option.fromNullable(System.getenv("VM_FUNKTIONS_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/funktions"
    is Some -> it.t
  }}

  val funktionDirectory: String = Option.fromNullable(System.getenv("VM_FUNKTION_DIRECTORY")).let { when(it) {
    is None -> "" // do something
    is Some -> it.t
  }}

  val modulesPath: String = Option.fromNullable(System.getenv("VM_MODULES_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/modules"
    is Some -> it.t
  }}

  val pluginsPath: String = Option.fromNullable(System.getenv("VM_PLUGINS_PATH")).let { when(it) {
    is None -> "${System.getProperty("user.dir")}/plugins"
    is Some -> it.t
  }}
}