package garden.bots.tools

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import garden.bots.core.Kompilo
import io.minio.MinioClient
import io.vertx.core.json.JsonObject
import java.io.File
import java.nio.file.Files
import java.nio.file.Paths

fun getFunktionInformation(funktionsPath: String, funktionToLoad: String): Try<DataFunktion> {
  return Try {
    val funktionDirectory = File("$funktionsPath/$funktionToLoad")
    val path = funktionDirectory.canonicalPath.toString()

    val jsonFile = File("$path/deploy.json").readText(Charsets.UTF_8)
    val deployInformation = JsonObject(jsonFile)
    val funktionName = deployInformation.getString("name")
    val funktionVersion = deployInformation.getString("version")
    val functionType = deployInformation.getString("type")

    DataFunktion(funktionName, funktionVersion, functionType)
  }
}

fun loadFunktionFromDisk(funktionsPath: String, funktionToLoad: String, kompilo: Kompilo): Try<DataFunktion> {
  return Try {
    // funktionToLoad is the path of the function in the directory functions
    val funktionDirectory = File("$funktionsPath/$funktionToLoad")
    val path = funktionDirectory.canonicalPath.toString()

    loadPluginsFromDisk("$funktionsPath/$funktionToLoad/plugins", kompilo)

    loadModulesFromDisk("$funktionsPath/$funktionToLoad/modules", kompilo)

    val jsonFile = File("$path/deploy.json").readText(Charsets.UTF_8)
    val deployInformation = JsonObject(jsonFile)
    val mainSourceCode = File("$path/main.kt").readText(Charsets.UTF_8)

    /* funktion compilation */
    val funktionName = deployInformation.getString("name")
    val funktionVersion = deployInformation.getString("version")
    val functionType = deployInformation.getString("type")

    println("🤖 > Compiling $funktionName($funktionVersion)")
    return kompilo.compileFunktion(deployInformation, mainSourceCode).let { it ->
      when(it) {
        is Failure -> {
          println("😡 > when compiling: ${it.exception.message}")
          it
        }
        is Success -> {
          println("🙂 > $funktionName($funktionVersion) ✅")
          Try{DataFunktion(funktionName, funktionVersion, functionType)}
        }
      }
    }
  }
}

fun loadFunktionFromObjectStorage(funktionsBucket: String, funktionStorageId: String, objectStorage: String, accessKey: String, secretKey: String, kompilo: Kompilo): Try<DataFunktion> {

  val funktionName = funktionStorageId.split(":")[0]
  val funktionVersion = funktionStorageId.split(":")[1]

  val strPath = "./tmp/$funktionName-$funktionVersion"
  val path = Paths.get(strPath)

  // --- Minio ---
  val initializeMinioClient = {
    val minioClient = MinioClient(objectStorage, accessKey, secretKey)
    val isExist = minioClient.bucketExists(funktionsBucket)
    if (isExist) {
      println("📦 > $funktionsBucket Bucket already exists.")
    } else {
      minioClient.makeBucket(funktionsBucket)
    }
    minioClient
  }

  val createTmpFunktionDirectory = {
    println("🙂 > funktionStorageId: $funktionStorageId")
    if(File(strPath).exists()) {
      val tmp = File(strPath)
      val entries = tmp.list()
      for (s in entries) { File(tmp.path, s).delete() }
      tmp.delete()
    }
    Files.createDirectory(path)
    println("🙂 > Directory created")
  }

  val loadJarPlugins = { minioClient: MinioClient ->

    println("📦 > Loading plugins(jar)")
    minioClient.listObjects(funktionsBucket, "$funktionStorageId:plugins").forEach {

      val objectName = it.get().objectName()
      val jarPath = "$strPath/${objectName.split(":").last()}"

      println("🌍 > Download $objectName to $jarPath")

      minioClient.getObject(funktionsBucket, objectName, jarPath)

      val plugin = File(jarPath)

      Try { kompilo.addJar(plugin.canonicalPath) }.let { it ->
        when(it) {
          is Failure -> {
            println("  😡 > when loading $plugin.canonicalPath")
            println("  😡 > ------------------------------------------------------------------------------")
            println("  😡 > ${it.exception.stackTrace}")
            println("  😡 > ------------------------------------------------------------------------------")
          }
          is Success -> println("  🙂 > ${plugin.nameWithoutExtension} loaded from ${plugin.canonicalPath}")
        }
      }
    }
    println("🎁 > plugins(jar) loaded")
  }

  val loadModules = { minioClient: MinioClient ->
    println("🔶 > Loading modules(kt)")
    minioClient.listObjects(funktionsBucket, "$funktionStorageId:modules").forEach {

      val objectName = it.get().objectName()
      val modulePath = "$strPath/${objectName.split(":").last()}"

      println("🌍 > Download $objectName to $modulePath")
      minioClient.getObject(funktionsBucket, objectName, modulePath)

      Try {
        val sourceCode = File(modulePath).readText(Charsets.UTF_8)
        /* module compilation */
        println("  🤖 > Compiling module: $modulePath")
        kompilo.compileModule(sourceCode).let { it ->
          when(it) {
            is Failure -> println("  😡 > when compiling: ${it.exception.message}")
            is Success<*> -> println("  🙂 > $modulePath 👍")
          }
        }
      }
    }
    println("🔶 > modules(kt) loaded")
  }

  val loadMainFunktion = { minioClient: MinioClient ->

    /* ===  Loading and compiling the main funktion === */
    val deployObjectName = "$funktionStorageId:deploy.json"
    val deployPath = "$strPath/deploy.json"
    minioClient.getObject(funktionsBucket, deployObjectName, deployPath)

    val mainObjectName = "$funktionStorageId:main.kt"
    val mainPath = "$strPath/main.kt"
    minioClient.getObject(funktionsBucket, mainObjectName, mainPath)

    val jsonFile = File("$strPath/deploy.json").readText(Charsets.UTF_8)
    val deployInformation = JsonObject(jsonFile)
    val mainSourceCode = File("$strPath/main.kt").readText(Charsets.UTF_8)

    /* funktion compilation */
    val funktionName = deployInformation.getString("name")
    val funktionVersion = deployInformation.getString("version")
    val functionType = deployInformation.getString("type")


    println("  🤖 > Compiling $funktionName($funktionVersion)")

    kompilo.compileFunktion(deployInformation, mainSourceCode).let { it ->
      when(it) {
        is Failure -> {
          println("  😡 > when compiling: ${it.exception.message}")
          it
        }
        is Success -> {
          println("  🙂 > $funktionName($funktionVersion) ✅")
        }
      }
    }
    DataFunktion(funktionName, funktionVersion, functionType)
  }

  return Try {
    val minioClient = initializeMinioClient()
    createTmpFunktionDirectory()
    loadJarPlugins(minioClient)
    loadModules(minioClient)

    loadMainFunktion(minioClient)
  }
}


